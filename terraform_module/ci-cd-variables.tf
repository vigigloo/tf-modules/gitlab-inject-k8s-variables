resource "gitlab_project_variable" "k8s-ca" {
  for_each          = toset(var.repositories)
  project           = each.value
  variable_type     = "file"
  protected         = false
  environment_scope = var.scope

  key   = "K8S_CA"
  value = var.cluster_ca_certificate
}

resource "gitlab_project_variable" "k8s-host" {
  for_each          = toset(var.repositories)
  project           = each.value
  protected         = false
  environment_scope = var.scope

  key   = "K8S_HOST"
  value = var.cluster_host
}

resource "gitlab_project_variable" "k8s-namespace" {
  for_each          = toset(var.repositories)
  project           = each.value
  protected         = false
  environment_scope = var.scope

  key   = "K8S_NAMESPACE"
  value = var.cluster_namespace
}

resource "gitlab_project_variable" "k8s-user" {
  for_each          = toset(var.repositories)
  project           = each.value
  protected         = false
  environment_scope = var.scope

  key   = "K8S_USER"
  value = var.cluster_user
}

resource "gitlab_project_variable" "k8s-token" {
  for_each          = toset(var.repositories)
  project           = each.value
  protected         = false
  environment_scope = var.scope

  key   = "K8S_TOKEN"
  value = var.cluster_token
}

resource "gitlab_project_variable" "base-domain" {
  for_each          = var.base-domain == null ? toset([]) : toset(var.repositories)
  project           = each.value
  protected         = false
  environment_scope = var.scope

  key   = "BASE_DOMAIN"
  value = var.base-domain
}
