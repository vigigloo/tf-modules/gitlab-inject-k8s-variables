variable "repositories" {
  type = list(string)

  description = "List of GitLab project IDs to add CI variables to."
}

variable "scope" {
  type = string

  description = "Environment scope of the variables."
}

variable "base-domain" {
  type    = string
  default = null

  description = "Value for the BASE_DOMAIN variable."
}

variable "cluster_user" {
  type = string

  description = "Value for the K8S_USER variable, the Service Account to connect to the Kubernetes API server."
}

variable "cluster_token" {
  type = string

  description = "Value for the K8S_TOKEN variable, the token to connect to the Kubernetes API server."
}

variable "cluster_namespace" {
  type = string

  description = "Value for the K8S_NAMESPACE variable, the namespace to install helm releases into."
}

variable "cluster_host" {
  type = string

  description = "Value for the K8S_HOST variable, the URL of the Kubernetes API server."
}

variable "cluster_ca_certificate" {
  type = string

  description = "Value for the K8S_CA variable, the CA certificate of the Kubernetes API server."
}
