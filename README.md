# Inject Kubernetes variables in GitLab projects

Manages CI/CD variables within GitLab projects to deploy them on Kubernetes.
The [vigigloo Helm pipeline fragment](https://gitlab.com/vigigloo/gitlab-pipeline-fragments/-/blob/v1/helm.yml) relies on these variables for deployment.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_gitlab"></a> [gitlab](#provider\_gitlab) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [gitlab_project_variable.base-domain](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable) | resource |
| [gitlab_project_variable.k8s-ca](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable) | resource |
| [gitlab_project_variable.k8s-host](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable) | resource |
| [gitlab_project_variable.k8s-namespace](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable) | resource |
| [gitlab_project_variable.k8s-token](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable) | resource |
| [gitlab_project_variable.k8s-user](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_base-domain"></a> [base-domain](#input\_base-domain) | Value for the BASE\_DOMAIN variable. | `string` | n/a | yes |
| <a name="input_cluster_ca_certificate"></a> [cluster\_ca\_certificate](#input\_cluster\_ca\_certificate) | Value for the K8S\_CA variable, the CA certificate of the Kubernetes API server. | `string` | n/a | yes |
| <a name="input_cluster_host"></a> [cluster\_host](#input\_cluster\_host) | Value for the K8S\_HOST variable, the URL of the Kubernetes API server. | `string` | n/a | yes |
| <a name="input_cluster_namespace"></a> [cluster\_namespace](#input\_cluster\_namespace) | Value for the K8S\_NAMESPACE variable, the namespace to install helm releases into. | `string` | n/a | yes |
| <a name="input_cluster_token"></a> [cluster\_token](#input\_cluster\_token) | Value for the K8S\_TOKEN variable, the token to connect to the Kubernetes API server. | `string` | n/a | yes |
| <a name="input_cluster_user"></a> [cluster\_user](#input\_cluster\_user) | Value for the K8S\_USER variable, the Service Account to connect to the Kubernetes API server. | `string` | n/a | yes |
| <a name="input_repositories"></a> [repositories](#input\_repositories) | List of GitLab project IDs to add CI variables to. | `list(string)` | n/a | yes |
| <a name="input_scope"></a> [scope](#input\_scope) | Environment scope of the variables. | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END_TF_DOCS -->